// @ts-check

/**
 * @param { Function | undefined | null } target 
 * @param { Function } f 
 * @return { Function }
 */
export function doAfter(target, f) {
    return /**@param { any[] } args @return { any } */(...args) => {
        if (!target) return f(...args, undefined);

        const result = target(...args);
        return f(...args, result);
    }
}