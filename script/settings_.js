// @ts-check

import { doAfter } from "./utils.js";

/**
 * @typedef { JSONSimpleValue | JSONArray | JSONObject  } JSONValue 
 * @typedef { string | number | boolean | null | undefined } JSONSimpleValue
 * @typedef { JSONValue[] } JSONArray
 * @typedef { {[name:string]: JSONValue } } JSONObject
 */

/**
 * Control your settings.
 */
export class Settings {
    /**
     * @type { [string, JSONValue][] }
     */
    options = [];
    
    /**
     * @type { {[option: string]: JSONValue} }
     */
    data = {};

    /**
     * add a option.
     * @param { string } name
     * @param { JSONValue } defaultValue
     * @returns { void } 
     */
    addOption(name, defaultValue) {
        this.options.push([name, defaultValue]);
    }

    /**
     * load all options.
     * @returns { void }
     */
    load() {
        for (const [option, defaultValue] of this.options) {
            this.data[option] = localStorage.getItem(option) ?? defaultValue;
        }
    }

    /**
     * save all options
     * @returns { void }
     */
    save() {
        for (const [option, _] of this.options) {
            localStorage.setItem(option, this.options[option]);
        }
    }

    /**
     * after being called once, the settings will be automatically saved back to localStorage when
     * the user close or refresh the page.
     * @returns { void }
     */
    saveOnExit() {
        doAfter(window.onbeforeunload, () => {
            this.save();
        });
    }
}