import { Clock } from "./clock.js";
import { localize } from "./localization.js"
import { render } from "./render.js";
import { loadAll as loadAllResources } from "./resource.js";
import { currentLanguage, initializeSettings, saveLanguage } from "./settings.js";
import { start as startCursorHide } from "./cursor.js";
import { Settings } from "./settings_.js";
import { loadAllSVG } from "./svg.js";
import { locked } from "./lock.js";
import { hidden } from "./hide.js";

async function pwa() {
    if (!navigator.serviceWorker?.register) 
        return console.warn("your browser doesn't support service worker!");
    const registration = await navigator.serviceWorker.register("/sw.js");
}

window.addEventListener("load", async () => {
    await pwa();
    await loadAllResources();
    globalThis.settings = new Settings();

    globalThis.Clock = Clock;
    Clock.load();
    initializeSettings();

    localize(currentLanguage);
    await loadAllSVG();
    startCursorHide();

    setInterval(render, 200);
    document.getElementById("content").hidden = false;
    document.body.removeChild(document.getElementById("loading"));
});

window.onbeforeunload = () => {
    Clock.save();
    saveLanguage();
};