import { localize } from "./localization.js";
import { parseColor } from "./render.js";
import { RESOURCES } from "./resource.js";

/**
 * @type { string }
 */
export let currentLanguage;

export function saveLanguage() {
    localStorage.setItem("language", currentLanguage);
}

export function initializeSettings() {
    const submitButton = document.getElementById("submitSettings");
    const closeButton = document.getElementById("closeSettings");
    const settingsModal = document.getElementById("settings");
    const backgroundColorInput = document.getElementById("backgroundColorInput");
    const textColorInput = document.getElementById("textColorInput");
    const openSettingsButton = document.getElementById("openSettings");
    const languageSelect = document.getElementById("languageSelect");
    
    currentLanguage = localStorage.getItem("language") || navigator.language.toLowerCase();
    if (!RESOURCES.LANG.has(currentLanguage)) currentLanguage = "en";

    const availableLanguages = Array.from(RESOURCES.LANG.keys());

    availableLanguages.forEach(language => {
        const optionElement = document.createElement("option");
        optionElement.value = language;
        optionElement.innerHTML = RESOURCES.LANG.get(language).data.__LANGUAGE_NAME;
        languageSelect.appendChild(optionElement);
    });

    languageSelect.value = currentLanguage;

    openSettingsButton.addEventListener("click", () => {
        if (settingsModal.hasAttribute("open")) {
            settingsModal.removeAttribute("open");
            return;
        }

        backgroundColorInput.value = globalThis.Clock.backgroundColor;
        textColorInput.value = globalThis.Clock.textColor;
        settingsModal.setAttribute("open", true);
    });

    submitButton.addEventListener("click", () => {
        settingsModal.removeAttribute("open");
        
        try {
            globalThis.Clock.backgroundColor = parseColor(backgroundColorInput.value);
        } catch (error) {
            console.warn(`Invalid background color '${backgroundColorInput.value}'`);
        }
        
        try {
            globalThis.Clock.textColor = parseColor(textColorInput.value);
        } catch (error) {
            console.warn(`Invalid text color '${textColorInput.value}'`);
        }

        const selectedLanguage = languageSelect.value;
        if (selectedLanguage !== currentLanguage) {
            localize(selectedLanguage);
            currentLanguage = selectedLanguage;
        }
        
        saveLanguage();
    });

    closeButton.addEventListener("click", () => {
        settingsModal.removeAttribute("open");
    });
}