/**
 * @type { boolean }
 */
export let locked = false;

let switched = false;

window.addEventListener("keydown", (ev) => {
    if (ev.key != "l" || switched) return; 
    locked = !locked;
    switched = true;
});

window.addEventListener("keyup", (ev) => {
    if (ev.key != "l") return;
    switched = false;
});

window.addEventListener("contextmenu", (ev) => {
    if (locked) ev.preventDefault();
});