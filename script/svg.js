// @ts-check

export async function loadAllSVG() {
    const elements = document.querySelectorAll("*[svg]");

    for (const element of elements) {
        const svg = element.getAttribute("svg");
        if (!svg) {
            console.warn("svg no value!");
            continue;
        }
        console.log(svg);
        element.innerHTML = await (await fetch(svg)).text();
    }
}