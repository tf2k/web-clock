/**
 * @type { boolean }
 */
export let hidden = false;

let switched = false;

window.addEventListener("keydown", (ev) => {
    if (ev.key != "h" || switched) return; 
    hidden = !hidden;
    switched = true;
});

window.addEventListener("keyup", (ev) => {
    if (ev.key != "h") return;
    switched = false;
});