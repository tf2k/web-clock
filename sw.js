const CACHE_NAME = "tunafish2k-web-clock-v1";
const CACHE_MANIFEST = {
    "css": {
        "index.css": null,
        "noto-sans-sc.css": null
    },
    "resources": {
        "images": {
            "custom.svg": null,
            "day.svg": null,
            "gitlab.svg": null,
            "night.svg": null,
            "settings.svg": null
        },
        "lang": {
            "en.json": null,
            "ja.json": null,
            "zh_cn.json": null
        },
        "manifest": {
            "lang.json": null
        },
        "pwa": {
            "app.json": null,
            "icon.png": null,
            "icon.svg": null
        }
    },

    "script": {
        "clock.js": null,
        "cursor.js": null,
        "index.js": null,
        "localization.js": null,
        "render.js": null,
        "resource.js": null,
        "settings_.js": null,
        "settings.js": null,
        "utils.js": null,
        "svg.js": null
    },
    "index.html": null,
    "favicon.ico": null,
    "": null
};

/**
 * @typedef { {[name:string]: null | CacheManifest} } CacheManifest
 */

/**
 * 
 * @param { CacheManifest } manifest
 * @param { string | void } prefix  
 * @returns { string[] }
 */
function parse(manifest, prefix) {
    if (!prefix) prefix = "/";
    const result = [];
    for (const file in manifest) {
        if (!manifest[file]) result.push(prefix + file);
        else for (const child of parse(manifest[file], `${prefix}${file}/`)) {
            result.push(child);
        }
    }
    return result;
}

self.addEventListener("install", event => {
    event.waitUntil((async () => {
        const cache = await caches.open(CACHE_NAME);
        const items = parse(CACHE_MANIFEST);
        await cache.addAll(items);
    })());
});

let lastFailure = 0;

self.addEventListener("fetch", event => {
    event.respondWith((async () => {
        console.log("fetch " + event.request.url);
        const cache = await caches.open(CACHE_NAME);
        try {
            if (Date.now() - lastFailure < 10000) throw new "give_up";
            const res = await fetch(event.request);
            cache.put(event.request, res.clone());
            return res;
        } catch (err) {
            if (err != "give_up")
                lastFailure = Date.now();
            const cachedResponse = await cache.match(event.request);
            if (!cachedResponse) {
                throw new Error(`${event.request.url} not found in cache!`);
            }
            return cachedResponse;
        }
    })());
});